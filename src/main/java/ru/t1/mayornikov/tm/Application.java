package ru.t1.mayornikov.tm;

import ru.t1.mayornikov.tm.api.ICommandRepository;
import ru.t1.mayornikov.tm.constant.ArgumentConst;
import ru.t1.mayornikov.tm.constant.CommandConst;
import ru.t1.mayornikov.tm.model.Command;
import ru.t1.mayornikov.tm.repository.CommandRepository;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import static ru.t1.mayornikov.tm.util.FormatUtil.formatBytes;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArguments(args);
        proccessCommands();
    }

    private static void proccessCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println();
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextline();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.err.println("Current program arguments are not correct...");
        System.exit(1);
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        final long memoryMax = Runtime.getRuntime().maxMemory();
        final long memoryFree = Runtime.getRuntime().freeMemory();
        final long memoryUse = memoryTotal - memoryFree;
        System.out.println("PROCESSORS: " + processorCount);
        System.out.println("MAX MEMORY: " + formatBytes(memoryMax));
        System.out.println("TOTAL MEMORY: " + formatBytes(memoryTotal));
        System.out.println("USAGE MEMORY: " + formatBytes(memoryUse));
        System.out.println("FREE MEMORY: " + formatBytes(memoryFree));
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.out.println("Current command are not correct...");
        System.out.println("Use 'help' for more information.");
    }

    private static void processCommand(final String argument) {
        switch (argument) {
            case (CommandConst.VERSION) : showVersion(); break;
            case (CommandConst.ABOUT) : showAbout(); break;
            case (CommandConst.INFO) : showSystemInfo(); break;
            case (CommandConst.HELP) : showHelp(); break;
            case (CommandConst.EXIT) : exit(); break;
            default : showErrorCommand();break;
        }
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case (ArgumentConst.VERSION) : showVersion(); break;
            case (ArgumentConst.ABOUT) : showAbout(); break;
            case (ArgumentConst.INFO) : showSystemInfo(); break;
            case (ArgumentConst.HELP) : showHelp(); break;
            default : showErrorArgument(); break;
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Mayornikov");
        System.out.println("E-mail: amayornikov@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        for (Command command: COMMAND_REPOSITORY.getCommands()) System.out.println(command);
    }

}